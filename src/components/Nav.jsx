import React, {useContext} from "react";
import ThemeContext from "../ThemeContext";
import {
    Link
} from "react-router-dom";
const Navbar = ({setKeyWord}) => {

    const {toggle} = useContext(ThemeContext);
    const handleSearch = (e) => {
        setKeyWord(e.target.value)
    };

    return (

        <div className='NavBar'>
            <span>My App</span>
            <input type='search' placeholder="Enter Your Keyword" onChange={handleSearch}/>
            <button onClick={toggle}>Toggle</button>
            <div className="links">
                <Link to="/">Home</Link>
                <Link to="/checkout">Checkout</Link>
            </div>
        </div>

    )
};

export default Navbar