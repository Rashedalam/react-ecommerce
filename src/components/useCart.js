import {useState} from "react";

const useCart = (init,products) => {

    const [cartItems, setCartItem] = useState(init);
    const addCartItem = (id) => {
        const result = products.find(item => id === item.id);

        setCartItem((cartItems) => {

            const items = cartItems.findIndex((item) => item.id === id);
            if (items === -1) {
                return [...cartItems, {...result, quantity: 1}]
            } else {
                return cartItems.map(item => item.id === id ? {...item, quantity: parseInt(item.quantity) + 1} : item)
            }

        });


    };

    const removeItem = (id) => {
        setCartItem((items) => {
            return items.filter((item) => item.id !== id)
        })
    };

    const clearCart = () => {
        const result = window.confirm("Are You Sure Want to Clear Your Cart");
        if (result === true) {
            setCartItem([]);
        }
    };

    return{
        cartItems,
        addCartItem,
        removeItem,
        clearCart
    }

};

export default useCart;