import React from "react";
import {useParams} from "react-router-dom"
import data from "../data";
import ListProduct from "./ListProduct";

const ProductDetails = () => {
    const {productId} = useParams();
    const product = data.find(p => p.id === parseInt(productId));
    return (
        <div className='checkout'>
            <h1>Product Details</h1>
            <ListProduct {...product} />
        </div>
    )
};

export default ProductDetails