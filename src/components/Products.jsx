import React, {useContext} from "react";
import ThemeContext from "../ThemeContext";
import ListProduct from "./ListProduct";



const Products = ({products, addCartItem}) => {
    const {dark} = useContext(ThemeContext);
    return (
        <div className={`products ${dark ? 'dark' : 'light'}`}>
            {
                products.map((product) => {
                    return <ListProduct {...product} key={product.id} addCartItem={addCartItem}/>
                })
            }
        </div>
    )
};

export default Products