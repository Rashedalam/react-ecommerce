import React, {useEffect, useState} from "react";
import Products from "./Products";
import Cart from "./Cart";
import useCart from "./useCart";
import data from "../data";


const Home = ({keyWord}) => {
    const [products, setProduct] = useState([...data]);
    const {
        cartItems,
        addCartItem,
        removeItem,
        clearCart
    } = useCart([], products);

    useEffect(() => {
        const results = data.filter(product => (product.title.includes(keyWord) || product.brand.includes(keyWord)));
        setProduct(results)
    }, [keyWord]);

    return (
        <>
            <Products products={products} addCartItem={addCartItem}/>
            <Cart cartItems={cartItems} removeItem={removeItem} clearCart={clearCart}/>
        </>
    )
};

export default Home