import React, {useState} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import './App.css';
import Navbar from "./components/Nav";
import ThemeContext from "./ThemeContext";
import Checkout from "./components/Checkout";
import ProductDetails from "./components/ProductDetails";
import Home from "./components/Home";

function App() {
    const [dark, setDark] = useState(false);
    const [keyWord, setKeyWord] = useState("");


    const toggleDark = () => {
        setDark(isDark => !isDark)
    };


    return (
        <ThemeContext.Provider value={{
            dark: dark, toggle: toggleDark
        }}>
            <div className={`App ${dark ? 'dark' : ''}`}>
                <Router>
                    <Navbar setKeyWord={setKeyWord}/>
                    <Switch>
                        <Route path="/checkout" component={Checkout}/>
                        <Route path="/product/:productId" component={ProductDetails}/>
                        <Route path="/" component={() => <Home keyWord={keyWord}/>}/>
                    </Switch>
                </Router>

            </div>
        </ThemeContext.Provider>
    );
}

export default App;
